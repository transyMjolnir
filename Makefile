#
# Makefile for BSD make (`bmake`)
#
SIZES_AND_RESOS= 128 72  64 36  32 18
FILENAME_PREFIX= transyMjolnir_
SRC= transyMjolnir.ps
OFILES=


.for size res in ${SIZES_AND_RESOS}
OFILES+= ${FILENAME_PREFIX}${size}x${size}.png
${FILENAME_PREFIX}${size}x${size}.png: ${SRC}
	./ps2png.sh $> $@ ${size} ${res}
.endfor


all: ${OFILES}

clean:
	rm *.png

.PHONY: all clean
.MAIN: all

