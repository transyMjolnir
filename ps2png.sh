#!/usr/bin/env bash

if [ -z "$1" -o -z "$2" ]; then
	echo "${0##*/}:  generate PNG raster image from PostScript file"  >&2
	echo "usage:  ${0##*/} <infile> <outfile> [outsize] [outres]"  >&2
	exit 1
fi


RES=72
BOX=128
if [ -n "$4" ]; then
	RES="$4"
fi
if [ -n "$3" ]; then
	BOX="$3"
fi

if ! echo "$BOX" |grep x; then
	BOX="${BOX}x${BOX}"
fi


gs -sDEVICE=pngalpha \
   -q \
   -dNOPAUSE \
   -sOutputFile="$2" \
   -r"$RES" \
   -g"$BOX" \
   "$1"

exit "$?"
