#!/usr/bin/env bash

run() {
	echo "$@"
	"$@"
	return "$?"
}

for i in "128 72" "64 36" "32 18"; do
	S="${i% *}"
	R="${i#$S}"
	run ./ps2png.sh transyMjolnir.ps transyMjolnir_${S}x${S}.png ${S} ${R}
done

